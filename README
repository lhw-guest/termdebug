Introduction
============

Termdebug is a set of utilities to record and replay the input and output of a
terminal program. Its main goal is to aid in developing and debugging terminal
programs. Other programs such as termrec/termplay, nethack-recorder/player and
script/scriptreplay only record the output. However, when debugging an
interactive terminal program, the input is often more important than the
output.

Motivation
==========

When developing an interactive terminal program, bugs can be very hard to track
down, as a specific sequence of actions may have triggered the bug. Usually one
stumbles upon a bug when doing undirected testing, and it is unclear which
sequence of actions triggered it. Moreover, it is often hard, if not
impossible, to recreate such a sequence from memory.

Termdebug allows you to record the input and output of your testing sessions
such that if you stumble upon a bug, you have a record of what you did. This
record can then be replayed to the program to recreate the exact circumstances
that triggered the bug.

A second use of termdebug is testsuites for interactive terminal programs. The
tdreplay program can verify that the output of the program is the same as the
recorded output. By building a set of testcases with known output you can
verify that changes to the program don't affect the output behaviour of the
program, or only those parts of the behaviour that you intended.

Utilities
=========

tdrecord           Starts a program and records its input and output.
tdreplay           Starts a program and replays the input of a recording.
tdview             Shows the output of a recording.
tdrerecord         Starts a program and replays the input of a recording,
					  recording the input and output to create a new recording.
tdcompare          Compares two recordings.

Prerequisites and Installation
==============================

Termdebug can provide optional (but recommended) functionality. This requires
at least ncurses, to add a visual comparison mode.

If further iconv, libunistring, Cairo and Pango are installed, pictures can be
generated of the visual differences. Finally, if pthread is available, tdreplay
will also include the option to generate a sequence of pictures showing the
progression of the terminal output.

To compile and install termdebug:

$ ./configure
or
$ ./configure --prefix=/usr
(see ./configure --help for more tuning options)
$ make all
$ make install

Reporting Bugs
==============

If you think you have found a bug, please check that you are using the latest
version of termdebug [http://os.ghalkes.nl/termdebug.html]. When reporting bugs,
please include a minimal example that demonstrates the problem.

Author
======

Gertjan Halkes <termdebug@ghalkes.nl>
