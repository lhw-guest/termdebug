/* Copyright (C) 2010,2012 G.P. Halkes
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 3, as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CURSES_HANDLING_H
#define CURSES_HANDLING_H

char *get_ti_string(const char *name);
int get_ti_bool(const char *name);
void call_setupterm(const char *name);
void sanitize_term(void);

#endif
