/* Copyright (C) 2013 G.P. Halkes
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 3, as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef WRITE_STRINGLIST_H
#define WRITE_STRINGLIST_H

#include "replay.h"
#include "window.h"

void win_write_data(window_t *win, char *data, size_t length);
void win_write_stringlist(window_t *win, StringListNode *string);
void setup_ti_strings(const char *term, const char *lang);

#endif