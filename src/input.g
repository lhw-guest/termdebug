/* Copyright (C) 2010-2013 G.P. Halkes
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 3, as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

%options "generate-lexer-wrapper generate-symbol-table lowercase-symbols";
%start parse, input;

{
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>
#include "replay.h"
#include "common.h"

static td_bool start_parsed;
static td_bool first_directive;
ExpNode *script;

extern int line_number;
extern const char *file_name;
extern char *yytext;

void LLmessage(int class) {
	switch (class) {
		case LL_MISSINGEOF:
			fatal("%s:%d: Unexpected %s at end of file\n", file_name, line_number, LLgetSymbol(LLsymb));
		case LL_DELETE:
			fatal("%s:%d: Unexpected %s\n", file_name, line_number, LLgetSymbol(LLsymb));
		default:
			fatal("%s:%d: Expected %s before %s\n", file_name, line_number, LLgetSymbol(class), LLgetSymbol(LLsymb));
	}
}

void *safe_calloc(size_t size) {
	void *retval = calloc(1, size);
	if (retval == NULL)
		fatal("Out of memory while reading script\n");
	return retval;
}

char *safe_strdup_remove_quotes(const char *string) {
	size_t size;
	char *retval = strdup(string + 1);

	if (retval == NULL)
		fatal("Out of memory while reading script\n");
	if ((size = strlen(retval)) > 0)
		retval[size - 1] = 0;
	return retval;
}

/** Convert a string from the input format to an internally usable string.
	@param string A @a Token with the string to be converted.
	@param descr A description of the string to be included in error messages.
	@return The length of the resulting string.

	The use of this function processes escape characters. The converted
	characters are written in the original string.
*/
static size_t parseEscapes(char *string) {
	size_t maxReadPosition = strlen(string);
	size_t readPosition = 0, writePosition = 0;
	size_t i;

	while(readPosition < maxReadPosition) {
		if (string[readPosition] == '\\') {
			readPosition++;

			if (readPosition == maxReadPosition)
				fatal("Interal error\n");

			switch(string[readPosition++]) {
				case 'n':
					string[writePosition++] = '\n';
					break;
				case 'r':
					string[writePosition++] = '\r';
					break;
				case '\'':
					string[writePosition++] = '\'';
					break;
				case '\\':
					string[writePosition++] = '\\';
					break;
				case 't':
					string[writePosition++] = '\t';
					break;
				case 'b':
					string[writePosition++] = '\b';
					break;
				case 'f':
					string[writePosition++] = '\f';
					break;
				case 'a':
					string[writePosition++] = '\a';
					break;
				case 'v':
					string[writePosition++] = '\v';
					break;
				case '?':
					string[writePosition++] = '\?';
					break;
				case '"':
					string[writePosition++] = '"';
					break;
				case 'x': {
					/* Hexadecimal escapes */
					unsigned int value = 0;
					/* Read at most two characters, or as many as are valid. */
					for (i = 0; i < 2 && (readPosition + i) < maxReadPosition && isxdigit(string[readPosition + i]); i++) {
						value <<= 4;
						if (isdigit(string[readPosition + i]))
							value += (int) (string[readPosition + i] - '0');
						else
							value += (int) (tolower(string[readPosition + i]) - 'a') + 10;
						if (value > UCHAR_MAX)
							fatal("%s:%d: Invalid hexadecimal escape sequence\n", file_name, line_number);
					}
					readPosition += i;

					if (i == 0)
						fatal("%s:%d: Invalid hexadecimal escape sequence\n", file_name, line_number);

					string[writePosition++] = (char) value;
					break;
				}
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7': {
					/* Octal escapes */
					int value = (int)(string[readPosition - 1] - '0');
					size_t maxIdx = string[readPosition - 1] < '4' ? 2 : 1;
					for (i = 0; i < maxIdx && readPosition + i < maxReadPosition && string[readPosition + i] >= '0' && string[readPosition + i] <= '7'; i++)
						value = value * 8 + (int)(string[readPosition + i] - '0');

					readPosition += i;

					string[writePosition++] = (char) value;
					break;
				}
				default:
					string[writePosition++] = string[readPosition - 1];
					break;
			}
		} else {
			string[writePosition++] = string[readPosition++];
		}
	}
	/* Terminate string. */
	string[writePosition] = 0;
	return writePosition;
}

}
%token ENV, START, WINDOW_SIZE, EXPECT, SEND, EXPECT_EXIT, INTERACT, NUMBER, STRING, EXPECT_SUSPEND, REQUIRE_VERSION;

input {
	ExpNode **current = &script;
	start_parsed = td_false;
	first_directive = td_true;
} :
	[
		command
		{
			*current = command;
			current = &(*current)->next;
			first_directive = td_false;
		}
	]+
	{
		check_script(file_name, script);
	}
;

command<ExpNode *> {
	StringListNode **next;
	LLretval = safe_calloc(sizeof(ExpNode));
	next = &LLretval->un.args;
} :
	ENV
	STRING
	{
		if (start_parsed)
			fatal("%s:%d: env directive after start directive\n", file_name, line_number);
		LLretval->type = EXP_ENV;
		LLretval->line = line_number;
		*next = safe_calloc(sizeof(StringListNode));
		(*next)->string = safe_strdup_remove_quotes(yytext);
		next = &(*next)->next;
	}
	STRING
	{
		*next = safe_calloc(sizeof(StringListNode));
		(*next)->string = safe_strdup_remove_quotes(yytext);
		(*next)->length = parseEscapes((*next)->string);
	}
|
	REQUIRE_VERSION
	NUMBER
	{
		if (!first_directive)
			fatal("%s:%d: require_version directive should be the first directive in the recording\n", file_name, line_number);
		LLretval->type = EXP_REQUIRE_VERSION;
		LLretval->line = line_number;
		LLretval->un.number = atoi(yytext);
		if (LLretval->un.number < 1)
			fatal("%s:%d: lowest possible require_version is 1\n", file_name, line_number);
	}
|
	START
	{
		if (start_parsed)
			fatal("%s:%d: Multiple start directives specified\n", file_name, line_number);
		start_parsed = td_true;
		LLretval->type = EXP_START;
		LLretval->line = line_number;
	}
	[
		STRING
		{
			*next = safe_calloc(sizeof(StringListNode));
			(*next)->string = safe_strdup_remove_quotes(yytext);
			(*next)->length = parseEscapes((*next)->string);
			next = &(*next)->next;
		}
	]+
|
	WINDOW_SIZE
	NUMBER
	{
		LLretval->type = EXP_WINDOW_SIZE;
		LLretval->line = line_number;
		LLretval->un.size.columns = atoi(yytext);
	}
	NUMBER
	{
		LLretval->un.size.rows = atoi(yytext);
	}
	[
		NUMBER
		{
			LLretval->un.size.delay = atoi(yytext);
		}
		[
			'>'
			NUMBER
			{
				LLretval->un.size.min_delay = atoi(yytext);
				if (LLretval->un.size.min_delay > LLretval->un.size.delay)
					LLretval->un.size.min_delay = LLretval->un.size.delay;
			}
		]?
	]?
|
	EXPECT
	{
		if (!start_parsed)
			fatal("%s:%d: expect directive before start directive\n", file_name, line_number);
		LLretval->type = EXP_EXPECT;
		LLretval->line = line_number;
	}
	[
		STRING
		{
			*next = safe_calloc(sizeof(StringListNode));
			(*next)->string = safe_strdup_remove_quotes(yytext);
			(*next)->length = parseEscapes((*next)->string);
			next = &(*next)->next;
		}
	]+
|
	SEND
	{
		if (!start_parsed)
			fatal("%s:%d: send directive before start directive\n", file_name, line_number);
		LLretval->type = EXP_SEND;
		LLretval->line = line_number;
	}
	[
		NUMBER
		{
			*next = safe_calloc(sizeof(StringListNode));
			(*next)->delay = atoi(yytext);
		}
		[
			'>'
			NUMBER
			{
				(*next)->min_delay = atoi(yytext);
				if ((*next)->delay < (*next)->min_delay)
					(*next)->min_delay = (*next)->delay;
			}
		]?
		STRING
		{
			(*next)->string = safe_strdup_remove_quotes(yytext);
			(*next)->length = parseEscapes((*next)->string);
			next = &(*next)->next;
		}
	]+
|
	EXPECT_EXIT
	NUMBER
	{
		if (!start_parsed)
			fatal("%s:%d: expect_exit directive before start directive\n", file_name, line_number);
		LLretval->type = EXP_EXPECT_EXIT;
		LLretval->line = line_number;
		LLretval->un.number = atoi(yytext);
	}
|
	EXPECT_SUSPEND
	{
		if (!start_parsed)
			fatal("%s:%d: expect_suspend directive before start directive\n", file_name, line_number);
		LLretval->type = EXP_EXPECT_SUSPEND;
		LLretval->line = line_number;
	}
|
	INTERACT
	{
		if (!start_parsed)
			fatal("%s:%d: interact directive before start directive\n", file_name, line_number);
		LLretval->type = EXP_INTERACT;
		LLretval->line = line_number;
	}
;
