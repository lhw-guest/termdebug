/* Copyright (C) 2013 G.P. Halkes
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 3, as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <pthread.h>

#include "common.h"
#include "window.h"

extern const char *picture_series;

typedef struct picture_queue_t picture_queue_t;
struct picture_queue_t {
	window_t *winA, *winB;
	char *name;
	picture_queue_t *next;
};

static picture_queue_t *picture_queue_head, *picture_queue_tail;
static pthread_t picture_thread;
static pthread_mutex_t picture_queue_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t picture_queue_cond = PTHREAD_COND_INITIALIZER;
static td_bool finished;

void enqueue_pictures(window_t *winA, window_t *winB, const char *name) {
	picture_queue_t *item = malloc(sizeof(picture_queue_t));

	if (item == NULL)
		return;

	item->winA = win_copy(winA);
	item->winB = win_copy(winB);
	item->name = port_strdup(name);
	item->next = NULL;

	if (item->winA == NULL || item->winB == NULL || item->name == NULL) {
		win_del(item->winA);
		win_del(item->winB);
		free(item->name);
		free(item);
		return;
	}

	pthread_mutex_lock(&picture_queue_lock);
	if (picture_queue_head == NULL) {
		picture_queue_head = picture_queue_tail = item;
	} else {
		picture_queue_tail->next = item;
		picture_queue_tail = item;
	}
	pthread_cond_signal(&picture_queue_cond);
	pthread_mutex_unlock(&picture_queue_lock);
}

static void *picture_writer_thead(void *arg) {
	picture_queue_t *tmp;

	(void) arg;

	pthread_mutex_lock(&picture_queue_lock);
	pthread_cond_signal(&picture_queue_cond);
	do {
		if (picture_queue_head == NULL)
			pthread_cond_wait(&picture_queue_cond, &picture_queue_lock);

		tmp = picture_queue_head;
		if (picture_queue_head != NULL)
			picture_queue_head = picture_queue_head->next;
		pthread_mutex_unlock(&picture_queue_lock);

		if (tmp != NULL) {
			win_write_image(tmp->winA, tmp->winB, tmp->name);
			win_del(tmp->winA);
			win_del(tmp->winB);
			free(tmp);
		}

		pthread_mutex_lock(&picture_queue_lock);
	} while (!finished || picture_queue_head != NULL);
	pthread_mutex_unlock(&picture_queue_lock);
	return NULL;
}

static void join_picture_thread(void) {
	void *retval;

	pthread_mutex_lock(&picture_queue_lock);
	finished = td_true;
	pthread_cond_signal(&picture_queue_cond);
	pthread_mutex_unlock(&picture_queue_lock);
	pthread_join(picture_thread, &retval);
}

void start_picture_writer_thread(void) {
	pthread_mutex_lock(&picture_queue_lock);
	pthread_create(&picture_thread, NULL, picture_writer_thead, NULL);
	pthread_cond_wait(&picture_queue_cond, &picture_queue_lock);
	pthread_mutex_unlock(&picture_queue_lock);
	atexit(join_picture_thread);
}
