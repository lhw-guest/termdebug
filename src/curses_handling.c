/* Copyright (C) 2010,2012-2013 G.P. Halkes
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 3, as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* We have all this in a separate file, because the curses header files
   clutter up the name space too much with macros and what not. This also
   means we have to duplicate some of the stuf from common.h. */
#include <curses.h>
#include <term.h>

#ifdef HAVE_STRDUP
#include <string.h>
#define port_strdup(_a) strdup(_a)
#else
char *port_strdup(const char *s);
#endif

void reset_tty(void);

char *get_ti_string(const char *name) {
	char *result = tigetstr(name);
	if (result == (char *) 0 || result == (char *) -1)
		return NULL;

	return port_strdup(result);
}

int get_ti_bool(const char *name) {
	return tigetflag(name);
}

void sanitize_term(void) {
	char *tistr;
	int errret;

	if (setupterm(NULL, 1, &errret) == ERR)
		return;

	tistr = get_ti_string("rmcup");
	if (tistr != NULL)
		putp(tistr);
	tistr = get_ti_string("rmacs");
	if (tistr != NULL)
		putp(tistr);
	tistr = get_ti_string("cvvis");
	if (tistr != NULL)
		putp(tistr);
}

void call_setupterm(const char *name) {
	setupterm(name, 1, 0);
}
