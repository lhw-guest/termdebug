/* Copyright (C) 2010,2013 G.P. Halkes
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 3, as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

%{
#include <string.h>
#include <errno.h>
#include <stdio.h>

#include "replay.h"
#include "common.h"
#include "input.h"
int line_number;
const char *file_name;
%}

%option noyywrap nodefault nounput noinput

%%
[ \t]
\n                          line_number++;
#.*
env                         return ENV;
start                       return START;
window_size                 return WINDOW_SIZE;
expect                      return EXPECT;
send                        return SEND;
expect_exit                 return EXPECT_EXIT;
expect_suspend              return EXPECT_SUSPEND;
interact                    return INTERACT;
require_version             return REQUIRE_VERSION;
[0-9]+                      return NUMBER;
\"([^\\"]|\\.)*\"           { int i; for (i = 0; i < yyleng; i++) if (yytext[i] == '\n') line_number++; return STRING; }
>                           return '>';
.                           fatal("%s:%d: Unexpected character '%c'\n", file_name, line_number, yytext[0]);

%%

void reset_lexer(const char *new_file_name) {
	if (yyin != NULL)
		fclose(yyin);

	if (new_file_name == NULL)
		return;

	if ((yyin = fopen(new_file_name, "r")) == NULL)
		fatal("Can't open input %s: %s\n", new_file_name, strerror(errno));
	file_name = new_file_name;
	line_number = 1;
}
