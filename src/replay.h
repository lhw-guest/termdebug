/* Copyright (C) 2010,2013 G.P. Halkes
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 3, as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REPLAY_H
#define REPLAY_H

typedef enum {
	EXP_START,
	EXP_ENV,
	EXP_WINDOW_SIZE,
	EXP_EXPECT,
	EXP_SEND,
	EXP_EXPECT_EXIT,
	EXP_INTERACT,
	EXP_EXPECT_SUSPEND,
	EXP_REQUIRE_VERSION
} ExpType;

typedef struct StringListNode StringListNode;
struct StringListNode {
	int length;
	char *string;
	StringListNode *next;
	int delay, min_delay;
};

typedef struct ExpNode ExpNode;
struct ExpNode {
	ExpType type;
	ExpNode *next;
	int line;
	union {
		StringListNode *args;

		struct {
			int columns;
			int rows;
			int delay;
			int min_delay;
		} size;

		int number;
	} un;
};

extern ExpNode *script;
void reset_lexer(const char *new_file_name);
#endif
