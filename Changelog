Version 2.2:
    - Fixed bug in configure script which prevented compilation with pthreads

Version 2.1:
    - Fixed incorrect output of window_size directive in tdrerecord
    - Fixed several print formatting issues
    - Reset the terminal to blocking mode after exit
    - Fixed several build issues with different combinations of available
      libraries

Version 2.0:
    - Added visual comparison mode, which emulates a terminal and compares the
      emulated outputs
    - Added a program to compare two recordings
    - Changed handling of expect in tdreplay, such that it now waits for the
      amount of time specified by the next send or window_size event
    - Added optional minimum delay to send events and window_size events to
      force a wait even if the key delay setting causes a smaller wait
    - Fixed incorrect printing of line numbers in parse errors

Version 1.2:
    - Added an option to tdrecord to prevent clearing of the DISPLAY
      environment variable
    - Fixed handling of octal escapes in strings for octal numbers starting
      with 4 through 7
    - Fixed handling of unknown escape sequences in strings
    - Fixed handling of -- for tdrecord

Version 1.1:
    - Added code to reset terminal to sane values after abort when replaying
      in display mode

Version 1.0:
    - Initial release
